from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
admin.site.register(ExpenseCategory)


class ExpenseCategoryAdmin:
    list_display = ("name", "owner")


admin.site.register(Account)


class AccountAdmin:
    list_display = ("name", "number", "owner")


admin.site.register(Receipt)


class ReceiptAdmin:
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
    )
