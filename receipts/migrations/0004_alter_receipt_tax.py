# Generated by Django 4.1.5 on 2023-01-20 04:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0003_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(
                blank=True, decimal_places=3, max_digits=10
            ),
        ),
    ]
