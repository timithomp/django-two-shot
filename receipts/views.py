from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
@login_required
def receipts_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts_list,
    }
    return render(request, "receipts/receipts_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            'form': form
        }
        return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    context = {
        "category_list": request.user.categories.all(),
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def accounts_list(request):
    context = {
        "accounts_list": request.user.accounts.all(),
    }
    return render(request, "receipts/accounts_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("categories")
    else:
        form = CategoryForm()
        context = {
            'form': form
        }
        return render(request, "receipts/create_category.html", context)



@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()
        context = {
            'form': form
        }
        return render(request, "receipts/create_account.html", context)
